# README #

This is an example for educational purposes of caching alembic both in maya interactive and from batch.

### To run the maya interactive version ###

(in maya script editor's python tab)
```python
# maya start up
path = r'D:\work\eclipse_workspace'  # <- path to where the tool folder is

# define env var for refernce path
import os
os.environ['EXAMPLE_PATH'] = os.path.join(path, 'abc_example')

# open scene
from maya import cmds
ref_scene = os.path.join(path, 'abc_example', 'refScene.ma')
cmds.file(ref_scene, o=1, f=1)

# select geo to export

cmds.select(cmds.ls('*:pCube1'))
cmds.select(cmds.ls('*:pSphere1'), add=True)

# export cache per namespace

import sys
sys.path.append(path)
from abc_example import lets_cache
reload(lets_cache)  # <- to take changes that you made

lets_cache.exportSelByNamespace()
```

### To run from DOS ###

(open dos shell)
```
cd /D D:\work\eclipse_workspace
batch_cache.bat .\refScene.ma "d:/test2/{}.abc"
```

### Who do I talk to? ###

* eduardo@eduardograna.com