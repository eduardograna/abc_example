'''
Created on 8 dic. 2019

@author: eduardo
'''
import sys
import os

import logging

logging.basicConfig()
logger = logging.getLogger('abc_example.batch_cache')

os.environ['EXAMPLE_PATH'] = os.path.dirname(__file__)

ERROR_MESSAGE = '''
Enter scene path and export path! for example:
batch_cache.bat "d:\\test\\refScene" "d:\\test\\{}.abc"
On export path {} will be replaced with namepsace
'''


def main(ma_path, export_path):
    logger.info('\nStarting maya\n\n')
    from maya import standalone, cmds  # @UnresolvedImport
    standalone.initialize()

    logger.info('\nloading scene {}\n\n'.format(ma_path))
    try:
        cmds.file(ma_path, o=True, f=True)
    except Exception, e:
        logger.warning('There were errors loading the scene!')

    visible_transforms = cmds.ls(visible=True, typ='transform')
    cmds.select(visible_transforms)

    import lets_cache
    logger.info('\nExporting caches to {}\n\n'.format(export_path))
    lets_cache.exportSelByNamespace(export_path=export_path)

    logger.info('\nDONE! Exporting caches to {}\n\n'.format(export_path))


if __name__ == '__main__':

    # parse arguments monkey way (checkout argparse for better ways)
    try:
        ma_path = sys.argv[1]
        export_path = sys.argv[2]
    except Exception, e:
        print ERROR_MESSAGE
        sys.exit(1)

    main(ma_path, export_path)
