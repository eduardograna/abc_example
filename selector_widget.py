'''
Created on 26 may. 2020


# maya start up
path = r'D:\work\eclipse_workspace'  # <- path to where the tool folder is

# define env var for refernce path
import os
os.environ['EXAMPLE_PATH'] = os.path.join(path, 'abc_example')

# open scene
from maya import cmds
ref_scene = os.path.join(path, 'ref_update', 'scenes', 'refScene.ma')
cmds.file(ref_scene, o=1, f=1)

# import and run script
import sys
sys.path.append(path)

from abc_example import selector_widget
reload(selector_widget)
ui = selector_widget.MainUI()
ui.show()

@author: eduardo
'''
from PySide2 import QtWidgets

import loadUiType
import os
from maya import cmds

DEFAULT_SEL = 'GEO_FINAL_GRP'

# create base class from ui file
MAIN_UI_FILE_REL_PATH = 'selector.ui'
MAIN_UI_FULL_PATH = os.path.join(os.path.dirname(__file__), MAIN_UI_FILE_REL_PATH)
MAIN_FORM_CLASS, MAIN_BASE_CLASS = loadUiType.loadUiType(MAIN_UI_FULL_PATH)


class MainUI(MAIN_FORM_CLASS, MAIN_BASE_CLASS):
    ''' This class implements the main ui '''

    def __init__(self, template_sel=None):

        # get maya main wndows as a qwidget so we can parent our ui
        parent = getMayaMainWindow()

        # configure parent class and apply ui definition
        super(MainUI, self).__init__(parent=parent)
        self.setupUi(self)

        # if there is something on template_sel, use that, if not,
        # use default value stored on DEFAULT_SET
        self.default_selection = template_sel or DEFAULT_SEL

        # connect on refresh button
        self.refresh_pushButton.clicked.connect(self.populateList)
        # connect enter pressed on selection line edit
        self.sel_lineEdit.returnPressed.connect(self.populateList)

        # connect on selection changed on list
        self.sel_listWidget.itemSelectionChanged.connect(self.showSelectionInScene)

    @property  # <- important this converts this function in a property getter
    def default_selection(self):
        ''' getter for the field'''
        # get the text from the line edit
        text = self.sel_lineEdit.text() or ''

        # split by commas in case there are many names
        return text.split(',')

    @default_selection.setter  # <- important this converts this function in a property setter
    def default_selection(self, value):
        ''' setter for the field'''
        self.sel_lineEdit.setText(value)

    def populateList(self):
        ''' populates the main list '''

        # clear list to avoid duplication on refresh
        self.sel_listWidget.clear()

        # find all elements per namespace that match the selection name
        item_list = self.findSelectionInScene()
        item_list.sort()  # sorting for display in order

        # add items to the list
        for item in item_list:

            # create item with a name and a parent widget (the list)
            list_item = QtWidgets.QListWidgetItem(item, parent=self.sel_listWidget)

            # save original name in a variable inside the item
            list_item.maya_object_name = item

            # add item to list and set it as selected
            self.sel_listWidget.addItem(list_item)
            list_item.setSelected(True)

    def findSelectionInScene(self):
        ''' find the maya scene transforms that matches the naming on every namespace'''

        # get all namespaces
        ns_list = self.getRefNamespaces()

        all_sel_list = []

        # loop thorugh all namespaces
        for ns in ns_list:

            # get the elements for this namespace
            sel_list = self.getDefaultSelectionForNs(ns)

            # add all together in the all_sel_list
            all_sel_list.extend(sel_list)

        return all_sel_list

    def getDefaultSelectionForNs(self, ref_ns):
        ''' get the element that match the select criteria for a namespace
        Args:
            ref_ns (str): the namespace name
        Returns:
            list[str]: the list of elements matching the criteria on namespace
        '''
        # read selectio tokens from ui
        search_list = self.default_selection

        # search for selection withing the namespace, one criteria at a time
        default_sel_list = []
        for search in search_list:

            # list transform matching this search criteria (get long name)
            found_list = cmds.ls(cmds.ls(search, r=1), type='transform', long=1)

            # filter to keep only the ones in this namespace
            found_for_ns = [e for e in found_list if self.getNameSpace(e) == ref_ns]

            # add them to the list of found for namespace
            default_sel_list.extend(found_for_ns)

        return default_sel_list

    def getRefNamespaces(self):
        ''' get all the namespace in current scene '''

        # list all ref nodes. Some won't be valid references. We will filter that later
        ref_node_list = cmds.ls(type='reference')

        ref_ns_list = []

        # go through all reference nodes
        for ref_node in ref_node_list:

            # lets try to get the namespace, if it fails, it's because its not a file reference
            try:
                ns = cmds.referenceQuery(ref_node, ns=1, shortName=1)
                ref_ns_list.append(ns)

            except Exception, e:  # @UnusedVariable
                pass  # beware, not always a good idea to ignore errors!

        return ref_ns_list

    def getNameSpace(self, element):
        ''' get the namespace of a maya element '''

        # clean name to keep only the short name
        name = element.split('|')[-1]

        # if no : on name, then it has no namespace
        if ':' not in name:
            return ''

        # if it does, we split the name and keep the first part
        ns = name.split(':')[0]
        return ns

    def showSelectionInScene(self):
        ''' selects in scene what is selected on the list '''

        # get selection from list
        selection_in_list = self.getListSelection()

        # select on maya scene
        cmds.select(selection_in_list, r=1)

    def getListSelection(self):
        ''' list elements selected on the list '''

        # get selected items
        selected_item_list = self.sel_listWidget.selectedItems()

        selection_list = []

        # loop through selected items
        for selected_item in selected_item_list:

            # get maya name form item (remember we stored it when creating the item)
            maya_object_name = selected_item.maya_object_name

            # add the maya name to the list
            selection_list.append(maya_object_name)

        return selection_list


def getMayaMainWindow():
    ''' Tries to get the maya main window
    Args:
        None
    Returns:
        QWidget or None: the maya main window or None if it could not be found
    '''

    try:
        # we need to wrap a pointer as a pyside widget so we need help!
        from shiboken2 import wrapInstance
        from maya import OpenMayaUI as omui

        # get opinter object
        ptr = omui.MQtUtil.mainWindow()

        # cast pointer to widget (may fail)
        main_window = wrapInstance(long(ptr), QtWidgets.QWidget)

    except Exception, e:  # @UnusedVariable
        main_window = None

    return main_window
