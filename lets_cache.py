'''
Created on 6 dic. 2019


# maya start up
path = r'D:\work\eclipse_workspace'  # <- path to where the tool folder is

# define env var for refernce path
import os
os.environ['EXAMPLE_PATH'] = os.path.join(path, 'abc_example')

# open scene
from maya import cmds
ref_scene = os.path.join(path, 'abc_example', 'refScene.ma')
cmds.file(ref_scene, o=1, f=1)

# select geo to export

cmds.select(cmds.ls('*:pCube1'))
cmds.select(cmds.ls('*:pSphere1'), add=True)

# export cache per namespace

import sys
sys.path.append(path)
from abc_example import lets_cache
reload(lets_cache)  # <- to take changes that you made

lets_cache.exportSelByNamespace()

@author: eduardo
'''
import os
import collections
# import abc_cache
# reload(abc_cache)
import subprocess


from maya import cmds, mel  # @UnresolvedImport

import logging
logging.basicConfig()
logger = logging.getLogger('abc_example.lets_cache')

ABC_EXPORT_PLUGIN = 'AbcExport'


def exportSelByNamespace(export_path=None):
    ''' Exports selected transform's meshed grouped by namespace in paths
    according to export path
    Args:
        export_path (str): a format str with {} to replace it with the namespace
    Returns:
        None
    '''

    # defualt export
    if export_path is None:
        export_path = 'd:/test/{}.abc'

    # list selection
    to_export = cmds.ls(sl=1)

    # group by namespace
    group_dict = collections.defaultdict(list)

    for sel in to_export:
        namespace = sel.split(':')[0]
        group_dict[namespace].append(sel)

    # per namespace create path and export
    start_frame = cmds.playbackOptions(q=True, min=True)
    end_frame = cmds.playbackOptions(q=True, max=True)
    logger.info('frame range is {} - {}'.format(start_frame, end_frame))

    exported_files = []
    for namespace, objs in group_dict.items():
        path = export_path.format(namespace)

        # check folder exists
        checkFolderExists(path)

        # exports the cache with helper function
        exportAbcCache(start_frame, end_frame, objs, path)

        logger.info('Exported namespace {} ({} roots) to {}'.format(namespace, len(objs), path))
        exported_files.append(path)

    # open folders in windows explorer
    folders_to_open = [os.path.dirname(p) for p in exported_files]
    folders_to_open_set = set(folders_to_open)
    for path in folders_to_open_set:
        cmd = 'explorer.exe "{}"'.format(path.replace('/', '\\'))
        subprocess.call(cmd)

    # log result
    for path in exported_files:
        logger.info('Exported {}'.format(path))


def checkFolderExists(path):
    ''' Check if the folder for a path already exists, if it does not, it creates it
    Args:
        path (str): full path to the file to check it's folder
    Returns:
        None
    '''
    dirname = os.path.dirname(path)
    if not os.path.isdir(dirname):
        os.makedirs(dirname)


def exportAbcCache(start, end, root_list, abc_path):
    ''' Exports an abc cache
    Args:
        start (int): start frame for the cache
        end (int): end frame for the cache
        root_list (list[str]): the list of roots to cache
        abc_path (str): full path to the abc file to export
    Returns:
        None
    '''
    root_list_str = ''.join([' -root {} '.format(r) for r in root_list])
    cmd = 'AbcExport  -j " -fr {start} {end} -writeVisibility -stripNamespaces {root_list_str} -uvWrite -file \\"{abc_path}\\" " ;'
    resolved_cmd = cmd.format(**locals())

    loadAlembicPlugins()
    mel.eval(resolved_cmd)


def loadAlembicPlugins():
    ''' load alembic plugins '''
    loadPlugin(pluginName=ABC_EXPORT_PLUGIN)


def loadPlugin(pluginName):
    ''' load a plugin
    Args:
        pluginName (str): the name of the plugin to load
    Returns:
        None
    '''
    if cmds.pluginInfo(pluginName, q=1, loaded=1):
        message = 'Already loaded plugin {}'.format(pluginName)
        logger.warning(message)
        return

    message = 'Loading plugin {}'.format(pluginName)
    logger.info(message)
    cmds.loadPlugin(pluginName)
